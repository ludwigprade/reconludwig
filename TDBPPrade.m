function [ Mu ] = TDBPPrade( Setup, Stage )
    Mu=zeros(Stage.N_steps);
    T_rrd= BuildRMatrix(Setup, Stage)/Setup.ca;
    nSamples=length(Setup.t);
    for p=1:Setup.DetNumber
        P=Setup.SigMat(:,p);
        dPdt=(P(1:nSamples-1)-P(2:nSamples))/Setup.t(2);
        Map=round(reshape(T_rrd(p,:)*Setup.SampleRate, Stage.N_steps,Stage.N_steps));
        Map(Map>nSamples-1) = nSamples-1;
        Mu=Mu+dPdt(Map);
    end

