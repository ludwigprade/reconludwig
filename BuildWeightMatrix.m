function [ W0 ] = BuildWeightMatrix( r_rd, Setup, Stage )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
tic;
    W0 = zeros(Setup.DetNumber*Setup.nFreq,Stage.N_vox);
    k=2*pi*Setup.FreqSpec./Setup.ca; %=2*pi/lambda with 1/lambda=f/c
    for f = 1:Setup.nFreq
        W0((1:Setup.DetNumber)+(f-1)*Setup.DetNumber,:)=-1j*Setup.ca*k(f)*exp(j*k(f)*r_rd)./r_rd;
    end     
    clear k;
    clear r_rd;
    disp(['W0 build in: ', num2str(toc)]);
end

