function Mu = BuildPhantom(MUMAP, Setup, Stage)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
    switch MUMAP;
            case 'RODS'
                [Stage.X, Stage.Y] = meshgrid(linspace(-Setup.R, Setup.R,Stage.N_steps),linspace(-Setup.R, Setup.R,Stage.N_steps)); %Grid of voxels
                Objects = [
        +0 -0 0.5 0.2;
        +3 +3 0.5 0.5;
        +0 -3 1.0 1;
        -3 +0  1.0 0.8;
        %0.005 -0.004 .0006 1.5;
     %   -Geo.R*0.69 +Geo.R*0.69 .0002 1;
        %+0.000 0.000 .0003 1;
        %-Geo.R*0.56 +Geo.R*0.69 .0002 1;
        ]*1e-3; % Soviele Objekte wie du willst: [position_x position_y radius absorption]
                Mu = ones(numel(Stage.X),1)*0.0;

                %OBJECTS = [];
                for k = 1 : size(Objects,1)
                    P0 = Objects(k,[1 2]);
                    if k == -1
                        ind0 = find((Stage.X-P0(1)).^2+(Stage.Y-P0(2)).^2 < Objects(k,3).^2 & (Stage.X-P0(1)).^2+(Stage.Y-P0(2)).^2 > (Objects(k,3)*0.7).^2);
                    else
                        ind0 = find((Stage.X-P0(1)).^2+(Stage.Y-P0(2)).^2 < Objects(k,3).^2);
                    end
                    Mu(ind0) = Objects(k,4);
                end
                clear Stage.X
                clear Stage.Y
            case 'HEX'
                MuSquare=zeros(Stage.N_steps);
                theta = 0:60:360;
                SideLength=1.5e-3;
                xv = (SideLength/Setup.Resolution)*cosd(theta)+Stage.N_steps/2;
                yv = (SideLength/Setup.Resolution)*sind(theta)+Stage.N_steps/2;
                for x=1:Stage.N_steps
                    for y=1:Stage.N_steps
                       MuSquare(x,y)=1*inpolygon(x,y,xv,yv);
                    end
                end
               % imagesc(MuSquare)
                Mu=[];
                for x=1:Stage.N_steps   
                    Mu= [Mu;MuSquare(:,x)];
                end
        case 'ANTENNA'
                MuSquare=zeros(Stage.N_steps);
                width=0.001;
                height=0.2;
                xv=[-width, width, width, -width]*Stage.N_steps+Stage.N_steps/2;
                yv=[height, height, -height, -height]*Stage.N_steps+Stage.N_steps/2;
                for x=1:Stage.N_steps
                    for y=1:Stage.N_steps
                       MuSquare(x,y)=1*inpolygon(x,y,xv,yv);
                    end
                end 
                Mu=[];
                for x=1:Stage.N_steps   
                    Mu= [Mu;MuSquare(:,x)];
                end
        case 'LENA'
            lena=imread('lena.jpg');
            lenasw=mean(lena,3);
            lenasw=lenasw/max(max(lenasw));
    end
        
end

