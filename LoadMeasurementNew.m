function [RHS,Setup] = LoadMeasurement( FileName )
    disp('Loading data'); tic;
    load(FileName);
    Dim=ndims(Results.Response);
    
    ScalerFrequencies=1;
    ScalerProjections=1;
    REFERENCE=1;
    
    Setup.FreqSpec=Results.FreqSpec(1:ScalerFrequencies:end);
    Setup.nFreq=length(Setup.FreqSpec);
    
    Setup.DetAng=Results.Angles(1:ScalerProjections:end)*pi/180;
    Setup.DetNumber=length(Setup.DetAng);
    if Dim==2;
        Setup.Response=Results.Response(1:ScalerFrequencies:end, 1:ScalerProjections:end);
        Setup.Reference=Results.Reference(1:ScalerFrequencies:end, 1:ScalerProjections:end);
    end
    if Dim==3
        Setup.Response=Results.Response(1:ScalerFrequencies:end, 1:ScalerProjections:end,:);
        Setup.Reference=Results.Reference(1:ScalerFrequencies:end, 1:ScalerProjections:end,:); 
    end
    
    PMatrix=zeros(Setup.nFreq, Setup.DetNumber);
    RefMat =zeros(Setup.nFreq, Setup.DetNumber);
    Setup.BW = max(Setup.FreqSpec)-min(Setup.FreqSpec);
    Setup.w = 2*pi*Setup.FreqSpec;
    
    tInt=Results.Setup.SweepTime/10;
    SamplingRate=Results.Setup.SamplingRate;     
    if Dim==3
        disp('time series found');
        for phi=1:Setup.DetNumber
            for f=1:Setup.nFreq
                PMatrix(f,phi)=mean(Setup.Response(f,phi,1:SamplingRate*tInt));
                if isfield(Results, 'Reference')
                    IMPULSERESPONSE=1;
                    RefMat(f,phi)=mean(Setup.Reference(f,phi,1:SamplingRate*tInt));
                end
            end
        end
    end
    if Dim==2  
        PMatrix=Setup.Response;
        disp('no time series found');
        if isfield(Results, 'Reference');
            IMPULSERESPONSE=1;
            for phi=1:Setup.DetNumber
                for f=1:Setup.nFreq
                    RefMat(f,phi)=Setup.Reference(f,phi);
                end
            end
        end
    end
    if IMPULSERESPONSE==1
        PMatrix=GetImpulseResponse(Setup, PMatrix);
    end
    if REFERENCE==1
        PMatrix=PMatrix./abs(RefMat);
    end
    
    PMatrix=transpose(PMatrix); 
    Setup.P=[];
    for f=1:Setup.nFreq
        Setup.P = [Setup.P;PMatrix(:,f)];
    end
    RHS = [real(Setup.P);imag(Setup.P)];
    disp(['Data loaded in: ', num2str(toc), ' s']);
end

