function PMatrix=GetImpulseResponse(Setup, PMatrix)
disp('Correcting for frequency response');   
    a_1 = 1;
    s_1 = 7.4877e+05;
    x_1 = 3.4451e+06;
    H = (a_1*exp(-(Setup.FreqSpec-x_1).^2/(2*s_1^2))).^(1/2);
    H(H<=0.1)=0.1;
    
    G=1./H;
    GMat=[];
    for p=1:Setup.DetNumber
        GMat=[GMat,G'];
    end
    
    PMatrix=GMat'.*PMatrix;   
end