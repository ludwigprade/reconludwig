function [RHS,Setup] = LoadMeasurement( FileName )
    load(FileName);
    Dim=ndims(Results.Response);
    
    ScalerFrequencies=1;
    ScalerProjections=1;
    
    Setup.FreqSpec=Results.FreqSpec(1:ScalerFrequencies:end);
    Setup.nFreq=length(Setup.FreqSpec);
    
    Setup.DetAng=Results.Angles(1:ScalerProjections:end)*pi/180;
    Setup.DetNumber=length(Setup.DetAng);
    if Dim==2;
        Setup.Response=Results.Response(1:ScalerFrequencies:end, 1:ScalerProjections:end);
        Setup.Reference=Results.Reference(1:ScalerFrequencies:end, 1:ScalerProjections:end);
    end
    if Dim==3
        Setup.Response=Results.Response(1:ScalerFrequencies:end, 1:ScalerProjections:end,:);
        Setup.Reference=Results.Reference(1:ScalerFrequencies:end, 1:ScalerProjections:end,:); 
    end
    
    PMatrix=zeros(Setup.nFreq, Setup.DetNumber);
    Setup.BW = max(Setup.FreqSpec)-min(Setup.FreqSpec);
    Setup.w = 2*pi*Setup.FreqSpec;
    
    tInt=Results.Setup.SweepTime;
    SamplingRate=Results.Setup.SamplingRate;
    
    %ImpulseResponse=Q(ones(size(Setup.P,1),1)*(wind./(0.05+wind.^2))).*Q;
    
    if Dim==3
        disp('time series found');
        for phi=1:Setup.DetNumber
            for f=1:Setup.nFreq
                if isfield(Results, 'Reference');
                    %disp('Reference found');
                    PMatrix(f,phi)=mean(Setup.Response(f,phi,1:SamplingRate*tInt))/mean(Setup.Reference(f,phi,1:SamplingRate*tInt));
                else
                    PMatrix(f,phi)=mean(Setup.Response(f,phi,1:SamplingRate*tInt));
                end
            end
        end
    else
        disp('no time series found');
        if isfield(Results, 'Reference');
            %disp('Reference found');
            for phi=1:Setup.DetNumber
                for f=1:Setup.nFreq
                    PMatrix(f,phi)=Setup.Response(f,phi);%/Setup.Reference(f,phi);
                end
            end
            PMatrix=transpose(PMatrix);
        else
            PMatrix=transpose(Setup.Response);
        end
        %Data.p =transpose(Setup.Response);
    end
   
    %PMatrix=GetImpulseResponse(Setup, PMatrix);
    
%     Setup.P=[];
%     for f=1:Setup.nFreq
%         Setup.P = [Setup.P;PMatrix(:,f)];
%     end
    Setup.P=PMatrix(:);
    %getimpulseresponse;
    %Setup.P  = (ones(size(Setup.P,1),1)*(wind./(0.05+wind.^2))).*Setup.P ;
    Setup.PMatrix=PMatrix;
    RHS = [real(Setup.P);imag(Setup.P)];
   
end

