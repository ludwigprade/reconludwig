function r_rd= BuildRMatrix(Setup, Stage)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    %tic;
    DistanceMatrix=zeros(Stage.N_steps);
    r_rd=zeros(Setup.DetNumber, Stage.N_vox);
    Ruler=linspace(-Setup.R, Setup.R,Stage.N_steps);
    for p=1:Setup.DetNumber
        for x=1:Stage.N_steps
            for y=1:Stage.N_steps
                DistanceMatrix(x,y)=sqrt((Stage.DetPositions(1,p)-Ruler(x))^2+(Stage.DetPositions(2,p)-Ruler(y))^2);
            end
        end
        
        r_rd(p,:)=DistanceMatrix(:);
    end
    clear DistanceMatrix;
    clear Ruler;
        %disp(['R build in: ', num2str(toc)]);
end

