function [ K ] = FFTBPPrade(Setup, Stage)
    d0 = 2*Setup.R/Stage.N_steps;
    f0 = Setup.FreqSpec(1);
    delta_f=Setup.FreqSpec(2)-Setup.FreqSpec(1);

    pl = exp(-1j*2*pi*f0*(0:Stage.N_steps-1)*d0/Setup.ca);
    alpha = (2*Setup.R*delta_f)/Setup.ca;
    q2 = 10;
    q1 = round(alpha*q2);
    for k = 1 : Setup.DetNumber
        ql(k,:) = exp(1j*(Setup.DetRad-Setup.R+d0)*Setup.w/Setup.ca);%*exp(-1j*2*pi*f0*Geo.d0/Data.ca(2));
        Rn(k,:) = Setup.DetRad-Setup.R+(1:Stage.N_steps)*d0;%-Geo.d0/2;
    end

    M = zeros(Setup.nFreq,Stage.N_steps);
    for m = 1 : Setup.nFreq
        M(m,:) = exp(-1j*2*pi*(m-1)*alpha*[0:Stage.N_steps-1]/(Stage.N_steps));
    end
    M = M*diag(pl);

    Q = reshape(-1j*conj(Setup.PMatrix), Setup.DetNumber, Setup.nFreq);
    Q = Q ./(ones(Setup.DetNumber,1)*Setup.w);
    Q = Q .* ql;

    Tem = zeros(Setup.DetNumber,q2*Stage.N_steps);
    Tem(:,1:q1:q1*(Setup.nFreq-1)+1) = Q; % For phase only do ./abs(Q)
    g0 = ifft(Tem,[],2);
    Dat = 2*real(g0(:,1:Stage.N_steps)*diag(1./pl))*q2*Stage.N_steps;
    Dat = max(Dat,0).*Rn/Setup.DetNumber;
    Datp = Dat';

    Length_T=length(Dat(1,:)); %preparing the mapping matrix
    T_rrd= BuildRMatrix(Setup, Stage);
    T_rrd=T_rrd-(Setup.DetRad-Setup.R);
    T_rrd=T_rrd/Setup.Resolution;
    T_rrd(T_rrd>Length_T) = Length_T;
    T_rrd(T_rrd<=1) = 1;
    T_rrd=round(T_rrd);

    Mu=zeros(1,Stage.N_vox); %actual back projection is happening here
    for p=1:Setup.DetNumber
        P=Dat(p,:);
        Mu=Mu+P(T_rrd(p,:));
%         pause(1);
%         title(p);
%         imagesc(reshape(Mu, Stage.N_steps, Stage.N_steps));  colorbar;
    end
    
    Ruler=linspace(-Setup.R, Setup.R,Stage.N_steps);
    n = Stage.N_steps;
    [x,y] = meshgrid(1:n,1:n);
    L1 = sqrt((x-1).^2+(y-1).^2);
    L2 = sqrt((x-1).^2+(y-n).^2);
    L3 = sqrt((x-n).^2+(y-1).^2);
    L4 = sqrt((x-n).^2+(y-n).^2);
    
    LL = min(min(min(L1,L2),L3),L4);
    LL = 0.858*LL / norm(LL);

I = NaN(n);
sig = reshape(Mu,n,n);
I = sig;
I3 = I;
I(isnan(I)) = mean(I(~isnan(I)));
F = fft2(I);

    F = F(:).*LL(:);

K = (max(0,real(ifft2(reshape(F,n,n)))));
K(isnan(I3)) = NaN;


end

