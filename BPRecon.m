function I=BPRecon(P,r_rd, Setup, Stage, NN_FLAG)
% function [Recon, Data, Geo] = BPRecon(Recon, Geo, Data, NN_FLAG)
tic; disp('Starting back-projection inversion');

%BP done in circular recon area
Setup.R=(sqrt(2)*Setup.R);
[Stage.X, Stage.Y] = meshgrid(-Setup.R:Setup.Resolution:Setup.R,-Setup.R:Setup.Resolution:Setup.R);
Stage.ind = find(Stage.X.^2+Stage.Y.^2 < Setup.R^2);
Stage.N_vox = numel(Stage.ind);
Stage.N_steps = size(Stage.X,1);

N_f=length(Setup.FreqSpec);
delta_f=Setup.FreqSpec(2)-Setup.FreqSpec(1);
Stage.DetPositions = [Setup.DetRad*cos(Setup.DetAng);Setup.DetRad*sin(Setup.DetAng)];%clear Det;

r_rd = BuildRMatrix(Setup, Stage);
    
%R0=zeros(length(Setup.DetAng),1);
%tried this for square recon area, but did not work
% R0=[];
% for theta = Setup.DetAng
%     if (theta <= 45)
%         R0 =[R0, Setup.DetRad-Setup.R/cos(theta*pi/180)];
%     elseif (theta > 45) & (theta <=90)
%         R0 =[R0, Setup.DetRad-Setup.R/sin((theta)*pi/180)];
%     elseif (theta > 90) & (theta <= 135)
%         R0 =[R0, Setup.DetRad-Setup.R/cos((90-theta)*pi/180)];
%     elseif (theta > 135) & (theta <=180)
%         R0 =[R0, Setup.DetRad+Setup.R/sin((90-theta)*pi/180)];  
%     elseif (theta > 180) & (theta <= 225)
%         R0 =[R0, Setup.DetRad-Setup.R/cos((180-theta)*pi/180)];
%     elseif (theta > 225) & (theta <=270)
%         R0 =[R0, Setup.DetRad+Setup.R/sin((180-theta)*pi/180)]; 
%     elseif (theta > 270) & (theta <= 315)
%         R0 =[R0, Setup.DetRad-Setup.R/cos((270-theta)*pi/180)];
%     elseif (theta > 315)
%         R0 =[R0, Setup.DetRad+Setup.R/sin((270-theta)*pi/180)];
%     end
% end

R0 = sqrt(DetPositions(1,:).^2+DetPositions(2,:).^2)-Setup.R;
alpha = (2*Setup.R*delta_f)/Setup.ca;

d0 = 2*Setup.R/(Stage.N_steps); % effective resolution. It is also Data.ca/(2 N_f delta_f)
f0 = Setup.FreqSpec(1);
pl = exp(-1j*2*pi*f0*(0:Stage.N_steps-1)*d0/Setup.ca);
for k = 1 : Setup.DetNumber
    ql(k,:) = exp(1j*(R0(k)+d0)*Setup.w/Setup.ca);%*exp(-1j*2*pi*f0*d0/Data.ca(2));
    Rn(k,:) = R0(k)+(1:Stage.N_steps)*d0;%-Geo.d0/2;
end

M = zeros(N_f,Stage.N_steps);
for m = 1 : N_f
    M(m,:) = exp(-1j*2*pi*(m-1)*alpha*(0:Stage.N_steps-1)/(Stage.N_steps));
end

Data.pl = pl;
M = M*diag(pl);
Recon.M = M;

%%
Q = reshape(-1j*conj(P), Setup.DetNumber, N_f);
Q = Q ./(ones(Setup.DetNumber,1)*Setup.FreqSpec*2*pi);
Q = Q .* ql;
if 1
    if alpha == 1
        q2 = 1;
    else
        q2 = 10;
    end
    q1 = round(alpha*q2);
    Recon.qs = [q1 q2];
    Tem = zeros(Setup.DetNumber,q2*Stage.N_steps);
    Tem(:,1:q1:q1*(N_f-1)+1) = Q; % For phase only do ./abs(Q)
    g0 = ifft(Tem,[],2);
    Dat = 2*real(g0(:,1:Stage.N_steps)*diag(1./pl))*q2*Stage.N_steps;
end

Dat = max(Dat,0).*Rn/Setup.DetNumber;
Dat = Dat*(Setup.Resolution/Setup.Resolution)^2;
%%
Recon.Dat = Dat;
Datp = Dat';

for k = 1 : Setup.DetNumber
    IND0{k} = max(round(Stage.N_steps*(r_rd(k,:)-R0(k))/(2*Setup.R)),1);
end

Mapping = zeros(Setup.DetNumber, Stage.N_vox);
for k = 1 : Setup.DetNumber
    Mapping(k,:) = IND0{k}+(k-1)*Stage.N_steps;
end

n = Stage.N_steps;
[x,y] = meshgrid(1:n,1:n);
L1 = sqrt((x-1).^2+(y-1).^2);L2 = sqrt((x-1).^2+(y-n).^2);L3 = sqrt((x-n).^2+(y-1).^2);L4 = sqrt((x-n).^2+(y-n).^2);
LL = min(min(min(L1,L2),L3),L4);
Stage.LL = 0.858*sqrt(2)*LL / norm(LL);

if Setup.DetNumber == 1
    solution = Datp(Mapping);
else
    solution = sum(Datp(Mapping),1);
end

%I = NaN(size(Stage.X));
I=NaN(Stage.N_steps, Stage.N_steps);
sig = solution;
I(Stage.ind) = sig;
I3 = I;
I(isnan(I)) = mean(I(~isnan(I)));
F = fft2(I);
if Setup.DetNumber > 1
    F = F(:).*Stage.LL(:);
end
K = (max(0,real(ifft2(reshape(F,size(Stage.X,1),size(Stage.X,1))))));
%%
K(isnan(I3)) = NaN;

if ~NN_FLAG
    Recon.I_FFT = K;
else
    Recon.I_NN = K;
end
I=K;
disp(['Done in: ', num2str(toc)]);
return
%% Plots for debugging
if 0
    figure
    t = SH;
    subplot(311)
    plot(Vec(:,t));hold on;plot(Mec*Dat(t,:)','r-');
    subplot(312)
    plot(abs(Q(t,:)));hold on;plot(abs(M*Dat(t,:)'),'r-');
    subplot(313)
    plot(Dat(t,:));
end

