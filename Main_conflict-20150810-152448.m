close all
clear all
format long
addpath('G:\Matlab\TimeDomainSimulation');

TDRUN=1;
FDRUN=0;

%Simulation Setup
Setup.ca = 1485; % Speed of sound
Setup.DetRad = 40e-3; %Detector ring radius

Setup.Resolution = 50e-6; % Inverse resolution
Setup.R =4e-3;% Reconstruction radius. Half of the side length of the square in the middle
Setup.nFreq=100;
Setup.fStart=0.1e6;
Setup.fStop=5e6;
Setup.FreqSpec=linspace(Setup.fStart,Setup.fStop,Setup.nFreq);
Setup.DetNumber=180;

%Setting the stage
Setup.DetAng=linspace(0,360*(1-1/Setup.DetNumber),Setup.DetNumber);
Setup.BW = max(Setup.FreqSpec)-min(Setup.FreqSpec);
Setup.w = 2*pi*Setup.FreqSpec;

Stage.N_steps = length(-Setup.R:Setup.Resolution:Setup.R);%(round(Setup.R*2/Setup.Resolution)); %number of voxels in each direction
Stage.N_vox=Stage.N_steps^2;
Stage.DetPositions = [Setup.DetRad*cos((pi/180)*Setup.DetAng);Setup.DetRad*sin((pi/180)*Setup.DetAng)];%clear Det;

%Doing the simulation
MuPhantom   = BuildPhantom('RODS', Setup, Stage);  
Ruler_sim=linspace(-Setup.R, Setup.R,Stage.N_steps);

if FDRUN==1
    %Doing FD Simulation
    [Setup] = FDMBSimulation(Setup, Stage ,MuPhantom);

    %Reconstruction Setup
    Setup.R=4e-3;
    Setup.Resolution = 50e-6;
    Stage.N_steps = length(-Setup.R:Setup.Resolution:Setup.R);%(round(Setup.R*2/Setup.Resolution)); %number of voxels in each direction
    Stage.N_vox=Stage.N_steps^2;
    Ruler_recon=linspace(-Setup.R, Setup.R,Stage.N_steps);

    %Doing the reconstruction
    r_rd = BuildRMatrix(Setup, Stage);
    W0   = BuildWeightMatrixLudwig(r_rd, Setup, Stage);
    lambdas=[0 logspace(-6,1,200)*norm(W0,'fro')];
    Linv = speye(Stage.N_vox ,Stage.N_vox );
    
    %Model based
    I_mb=[real(W0);imag(W0)]'*[real(Setup.P);imag(Setup.P)];
    I_vec = xfmt_lsqr_hybrid([real(W0);imag(W0)], Linv, RHS,lambdas(140), 200); %Recon.lambda
    
    %Back projection
    I_bp = FFTBPPrade(Setup, Stage);
%%
    
    subplot(2,2,1);
    imagesc(Ruler_sim,Ruler_sim,reshape(MuPhantom, Stage.N_steps,Stage.N_steps));    
    title('Phantom');
    subplot(2,2,3);
    imagesc(Ruler_recon,Ruler_recon,reshape(I_mb, Stage.N_steps, Stage.N_steps));
    title('FD Model based');
    subplot(2,2,4);
    imagesc(Ruler_recon,Ruler_recon,reshape(I_bp,Stage.N_steps,Stage.N_steps));
    title('FD back projection');
end

if TDRUN==1
    %Doing TD Simulation
    [Setup] = TDSimulation(Stage, Setup, MuPhantom);

    %Reconstruction Setup
    Setup.R=4e-3;
    Setup.Resolution = 50e-6;
    Stage.N_steps = length(-Setup.R:Setup.Resolution:Setup.R);%(round(Setup.R*2/Setup.Resolution)); %number of voxels in each direction
    Stage.N_vox=Stage.N_steps^2;
    Ruler_recon=linspace(-Setup.R, Setup.R,Stage.N_steps);

    %Doing the reconstruction
    r_rd = BuildRMatrix(Setup, Stage);
    W0   = BuildWeightMatrixLudwig(r_rd, Setup, Stage);
    lambdas=[0 logspace(-6,1,200)*norm(W0,'fro')];
    Linv = speye(Stage.N_vox ,Stage.N_vox );
    
    %I_mb=reshape([real(W0);imag(W0)]'*[real(Setup.P);imag(Setup.P)], Stage.N_steps, Stage.N_steps);
    I_vec = xfmt_lsqr_hybrid([real(W0);imag(W0)], Linv, RHS,lambdas(140), 200); %Recon.lambda

    I_bp = FFTBPPrade(Setup, Stage);
    
    [I_td,X,Y] = backproject_luis(Stage, Setup,'full',0,0);
    
    subplot(2,2,1);
    axis equal
    imagesc(Ruler_sim,Ruler_sim,reshape(MuPhantom, Stage.N_steps,Stage.N_steps));    
    title('Phantom');
    
    subplot(2,2,2);
    imagesc(Ruler_recon,Ruler_recon,I_td);
    title('TD back projection');
    
    subplot(2,2,3);
    imagesc(Ruler_recon,Ruler_recon,I_mb);
    title('FD Model based');
    
    subplot(2,2,4);
    imagesc(Ruler_recon,Ruler_recon,reshape(I_bp,Stage.N_steps,Stage.N_steps));
    title('FD back projection');
    

end