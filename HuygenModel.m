% addpath('ReconLudwig')
% 
% %Simulation Setup
% Setup.ca = 1485; % Speed of sound
% Setup.DetRad = 40e-3; %Detector ring radius
% 
% Setup.Resolution = 20e-6; % Inverse resolution
% Setup.R =4e-3;% Reconstruction radius. Half of the side length of the square in the middle
% Setup.nFreq=100;
% Setup.fStart=0.1e6;
% Setup.fStop=5e6;
% Setup.FreqSpec=linspace(Setup.fStart,Setup.fStop,Setup.nFreq);
% Setup.DetNumber=180;
% 
% %Setting the stage
% Setup.DetAng=linspace(0,360*(1-1/Setup.DetNumber),Setup.DetNumber);
% Setup.BW = max(Setup.FreqSpec)-min(Setup.FreqSpec);
% Setup.w = 2*pi*Setup.FreqSpec;
% Stage.DetPositions = [Setup.DetRad*cos((pi/180)*Setup.DetAng);Setup.DetRad*sin((pi/180)*Setup.DetAng)];%(x;y)
% 
% Stage.N_steps = length(-Setup.R:Setup.Resolution:Setup.R);%(round(Setup.R*2/Setup.Resolution)); %number of voxels in each direction
% Stage.N_vox=Stage.N_steps^2;
% 
% %Doing the simulation
% MuPhantom   = reshape(BuildPhantom('RODS', Setup, Stage), Stage.N_steps,Stage.N_steps);  
% Ruler_sim=linspace(-Setup.R, Setup.R,Stage.N_steps);
% D=zeros(Stage.N_steps, Stage.N_steps, Setup.nFreq);
% [X,Y] = meshgrid(Ruler_sim,Ruler_sim);
% k=2*pi*Setup.FreqSpec./Setup.ca;
% 
% SigMat=zeros(Setup.DetNumber,Setup.nFreq);
% for p=1:Setup.DetNumber
%     disp(['Projection No. ', num2str(p)]);
%     R = sqrt((X-Stage.DetPositions(1,p)).^2+(Y-Stage.DetPositions(2,p)).^2);
% 	for f=1:Setup.nFreq
%         SigMat(p,f)=sum(sum(MuPhantom.*(-1j*Setup.FreqSpec(f)/Setup.ca).*((exp(1j*k(f).*R)./R))));
%     end
% end
% 

%%k-inversion code
%%highly experimental and top secret
clear all;

%load some data
load('G:\Matlab\MatlabDAQSoftware\4mmHex_2OD_Clear_2015.6.24_11.12.1.079.mat');
SigMat=(Results.Response)';
Setup.DetAng=Results.Angles;
Setup.DetNumber=length(Results.Angles);
Setup.FreqSpec=Results.FreqSpec;
Setup.nFreq=length(Setup.FreqSpec); %rename some stuff

%Recon Setup
Setup.ca = 1526; % Speed of sound
Setup.DetRad = 0.035; %Detector ring radius   

%Define the region of interest
Setup.Resolution = 200e-6; % Inverse resolution
Setup.R =10e-3;% Reconstruction radius. Half of the side length of the square in the middle
dX=0;
dY=0;

%Set the stage
Stage.DetPositions = [Setup.DetRad*cos((pi/180)*Setup.DetAng);Setup.DetRad*sin((pi/180)*Setup.DetAng)];%(x;y)
Stage.N_steps = length(-Setup.R:Setup.Resolution:Setup.R);%(round(Setup.R*2/Setup.Resolution)); %number of voxels in each direction
Stage.N_vox=Stage.N_steps^2;
Ruler=linspace(-Setup.R, Setup.R,Stage.N_steps);
[X,Y] = meshgrid(Ruler,Ruler);
X=X+dX;
Y=Y+dY;
Recon=zeros(Stage.N_steps,Stage.N_steps);
R=zeros(Stage.N_steps,Stage.N_steps,Setup.DetNumber);
k=2*pi*Setup.FreqSpec./Setup.ca;

for p=1:Setup.DetNumber
    R(:,:,p) = sqrt((X-Stage.DetPositions(1,p)).^2+(Y-Stage.DetPositions(2,p)).^2); %distance between the detector and each voxel
end
tic;
for p=1:Setup.DetNumber
        for f=1:Setup.nFreq
             Recon=Recon+SigMat(p,f).*(exp(-1j*k(f).*R(:,:,p))./R(:,:,p));
        end
end
toc;
imagesc(Ruler+dX, Ruler+dY, abs(Recon));
axis equal;
