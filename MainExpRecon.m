%close all
clear all
format long

MODELBASED=1;
BACKPROJECTION=0;

% %%%%% DO NOT TOUCH!!!
% [RHS, Setup]=LoadMeasurement('G:\Matlab\MatlabDAQSoftware\4mmHex_2OD_Clear_2015.6.24_11.12.1.079.mat');
% Setup.DetRad = 0.3603;
% Setup.ca = 1526; % Speed of sound
% %%%%% DO NOT TOUCH!!!

% Setup.DetRad = 0.059; %Detector ring radius   
% Setup.ca = 1485; % Speed of sound

%%%% DO NOT TOUCH!!!
%[RHS,Setup]=LoadMeasurement('G:\MeasurementFiles\IQ-Tomography\RotTransDoubleRod_2015.3.25_17.0.6.258.mat');
[RHS,Setup]=LoadMeasurement('G:\MeasurementFiles\IQ-Tomography\RotTransDoubleRod_2015.3.25_17.0.6.258.mat');
Setup.DetRad = 0.03726; %Detector ring radius   
%Setup.DetRad = 0.03738; %Detector ring radius   
Setup.ca = 1485; % Speed of sound
%%%% DO NOT TOUCH!!!


% Setup.DetRad = 0.3565;
% Setup.ca = 1526; % Speed of sound

Setup.Resolution = 50e-6; % Inverse resolution
Setup.R = 4e-3;% Reconstruction radius. Half of the side length of the square in the middle

Stage.N_steps = length(-Setup.R:Setup.Resolution:Setup.R);%(round(Setup.R*2/Setup.Resolution)); %number of voxels in each direction
Stage.N_vox=Stage.N_steps^2;
Ruler_recon=linspace(-Setup.R, Setup.R,Stage.N_steps);

%Setting the stage
Setup.DetAng=linspace(0,360*(1-1/Setup.DetNumber),Setup.DetNumber);
Setup.BW = max(Setup.FreqSpec)-min(Setup.FreqSpec);
Setup.w = 2*pi*Setup.FreqSpec;
Stage.DetPositions = [Setup.DetRad*cos((pi/180)*Setup.DetAng);Setup.DetRad*sin((pi/180)*Setup.DetAng)];%clear Det;
if MODELBASED==1
tic;
    %preparing the recon
    r_rd = BuildRMatrix(Setup, Stage); %detector pair matrix
    W0   = BuildWeightMatrix(r_rd, Setup, Stage); %weight matrix

    lambdas=[0 logspace(-6,1,200)*norm(W0,'fro')]; %regularisation parameter
    Linv = speye(Stage.N_vox ,Stage.N_vox ); %

    %Doing the reconstruction
    tic;I_vec=[real(W0);imag(W0)]'*[real(Setup.P);imag(Setup.P)];disp(['RC in: ', num2str(toc), ' s']);
    %I_vec = xfmt_lsqr_hybrid([real(W0);imag(W0)], Linv, [real(Setup.P);imag(Setup.P)],lambdas, 50); %Recon.lambda
    IMat=max(reshape(I_vec, Stage.N_steps, Stage.N_steps),-1e99);
    imagesc(Ruler_recon,Ruler_recon,IMat);
    colormap('gray');
    colorbar;
end

if BACKPROJECTION==1
    Setup.DetRad = 0.358;
    I_bp = FFTBPPrade(Setup, Stage);
    imagesc(Ruler_recon,Ruler_recon,I_bp);
end



