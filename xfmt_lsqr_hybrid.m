function x = xfmt_lsqr_hybrid(W,Linv,b,lams,maxits)
tic;
FLAG = ~isfield(W, 'G_sd');
if FLAG
    n_proj = 1;
else
    n_proj = length(W.use_data);
    n_voxels = W.n_voxels;
end
if isempty(Linv)
    Linv = speye(W.n_voxels);
end
EXP = 1;
nv = size(W, 2);
%b = max(b,0);

%% Finding norm vector
%
NormVector = speye(nv);
% if 0 % 4T1 compression
%
%     if ~EXP
%         %error('Shouldnt be here')
%         NormVector = zeros(nv,1);
%         for proj = 1 : length(W.G_sd)
%             for src = 1 : size(W.G_sd{proj},2)
%                 vsm = W.G_sm{proj}(:,src);
%                 vsd = W.G_sd{proj}(:,src);
%                 Mmd = W.G_md{proj};
%                 w = vsm*(1./vsd').*Mmd;
%                 w(:,~W.use_data{proj}(:,src)) = 0;
%                 w(w < 0) = 0;
%                 NormVector = NormVector+sum(abs(w).^2,2);
%             end
%         end
%         f = 1./sqrt(NormVector);
%         %f = f/max(f);
%         %save fphant f
%         %max(f)
%         %f(f > 0.4) = 0;
%         %f = ones(size(f));
%         NormVector = spdiags(f, 0, nv, nv);
%         for proj = 1 : length(W.G_sd)
%             W.G_md{proj} = NormVector*W.G_md{proj};
%         end
%         1;
%     else
%f = zeros(nv, 1);
%for k = 1 : nv
%    f(k) = sum(abs(W(:,k)));
%end
%f = 1./(f);
%f = f / max(f);
%NormVector = spdiags(f, 0, nv, nv);
%W = W * NormVector;
%     end
% end
%% Requires REGTOOLS functions: l_corner.m and dependencies (i.e. the spline toolbox).

lamsize=length(lams);

%% general initialization
if FLAG % then explicit representation
    m = size(W,2);
    bet = norm(b);
    b=reshape(b,size(b,1)*size(b,2),1);
    u=b/bet;
    vec = u;
    n_voxels = m;
    
else
    m = nv;
    bet=norm(b(find(xfmt_cells_to_vector(W.use_data))));
    u=b/bet;
    vec = xfmt_vector_to_cells(u, W.G_sd);
end

% Transpose product delivers a vector containing as many entries as there
% are voxels.
if FLAG
    result = (W'*vec);
else
    result = zeros(n_voxels,1);
    for proj=1:n_proj,
        G_sm = W.G_sm{proj};
        G_md = W.G_md{proj};
        v = vec{proj};
        x = W.use_data;
        use_data = logical(x{proj});
        
        v(use_data) = v(use_data)./ (W.G_sd{proj}(use_data));
        v(~use_data) = 0;
        if ~length(find(use_data))
            continue
        end
        for src = 1:size(G_sm,2)
            result(:) = result(:) + G_sm(:,src) .* (G_md * v(:,src));
        end;
    end;
end
tmp = result;
tmp = Linv'*tmp;
alph=norm(tmp); v=tmp/alph;

x = zeros(m, lamsize);
w = repmat(v, [1 lamsize]);
phi_bar = zeros(1,lamsize) + bet;
ro_bar = zeros(1,lamsize) + alph;

% for k=1:lamsize
%     x(:,k)=zeros(m,1);
%     w(:,k)=v;
%     phi_bar(k)=bet;
%     ro_bar(k)=alph;
% end

%%%%%% START MAIN LOOP
for i=1:maxits
    %% bidiagonalization step - same for all lambda, so no loop over lambda here
    
    %
    % the normal product calculates a result with as many entries as there
    % are detectors
    if FLAG
        tmp = W*v-alph*u;
    else
        result = W.G_sd;
        vec = Linv*v;
        vec = spdiags(vec,0,n_voxels,n_voxels);
        for proj=1:n_proj,
            
            xt = W.use_data;
            use_data = logical(xt{proj});
            if ~length(find(use_data))
                continue
            end
            
            tmp = W.G_md{proj}' * (vec * W.G_sm{proj});
            tmp(use_data) = tmp(use_data) ./ (W.G_sd{proj}(use_data));
            tmp(~use_data) = 0;
            result{proj} = tmp;
        end;
        result = xfmt_cells_to_vector(result);
        tmp=result-alph*u;
    end
    
    %bet=norm(tmp);
    if FLAG
        bet=norm(tmp);
        u=tmp/bet;
        vec = u;
    else
        bet=norm(tmp(find(xfmt_cells_to_vector(W.use_data))));
        u=tmp/bet;
        vec = xfmt_vector_to_cells(u, W.G_sd);
    end
    %        tmp=W.'*u - bet*v;
    
    
    result = zeros(n_voxels,1);
    if FLAG
        tmp=W.'*u - bet*v;
    else
        for proj=1:n_proj,
            G_sm = W.G_sm{proj};
            G_md = W.G_md{proj};
            vt = vec{proj};
            xt = W.use_data;%get(W,'use_data');
            
            use_data = logical(xt{proj});
            if ~length(find(use_data))
                continue
            end
            vt(use_data) = vt(use_data) ./ (W.G_sd{proj}(use_data));
            vt(~use_data) = 0;
            
            for src = 1:size(G_sm,2)
                result(:) = result(:) + G_sm(:,src) .* (G_md * vt(:,src));
            end;
        end;
        result=Linv'*result;
        tmp=result - bet*v;
    end
    
    alph=norm(tmp); v=tmp/alph;
    %% construct and apply rotations for each lambda
    warning('off','MATLAB:dividebyzero');
    
    %% first rotate lambda out
    ro_tilde = sqrt(ro_bar.^2 + lams.^2);
    c_hat = ro_bar ./ ro_tilde;
    s_hat = lams ./ ro_tilde;
    phi_tilde = c_hat .* phi_bar;
    psi = s_hat .* phi_bar;
    
    %% next rotate to upper bidiag
    ro = sqrt(ro_tilde.^2 + bet.^2);
    c = ro_tilde./ro; s = bet./ro;
    theta = s .* alph;
    ro_bar = -c .* alph;
    phi = c .* phi_tilde;
    phi_bar = s .* phi_tilde;
    
    % update x(lam) and w(lam)
    k = isnan(phi);
    phi(k) = 0; ro(k) = 1;
    k = (ro == 0); ro(k) = 1; phi(k) = 0;
    %x = max(x,0);
    x = x + w * spdiags((phi./ro)', 0, lamsize, lamsize);
    
    
    w = repmat(v, [1 lamsize]) - w * spdiags((theta./ro)', 0, lamsize, lamsize);
    
    if any(isnan(x(:)));
        keyboard; end;
    
end
x = Linv*x;%
%x = max(x,0);

x = NormVector*x;
disp(['Solving done in: ', num2str(toc)]);