function [Setup] = FDMBSimulation(Setup, Stage ,Mu)
    tic;disp('Starting simulating data');
 
    r_rd = BuildRMatrix(Setup, Stage);
    W0   = BuildWeightMatrix(r_rd, Setup, Stage);

    Setup.P= W0*Mu;  
    Setup.PMatrix=reshape(Setup.P, Setup.DetNumber, Setup.nFreq);
    %RHS = [real(Setup.P);imag(Setup.P)];
    disp(['Simulation done in: ', num2str(toc)]);
    Setup = IQModulate(Setup,Stage);
end

