function [ Setup ] = IQModulate(Setup,Stage)
    SigMat=[];
    for p=1:Setup.DetNumber
        SigMat=[SigMat;fftshift(ifft(Setup.PMatrix(p,:),Stage.N_steps))];
    end
    Setup.SigMat=real(SigMat);
    Setup.t=linspace(Setup.Resolution/Setup.ca, Stage.N_steps*Setup.Resolution/Setup.ca, Stage.N_steps);
end
